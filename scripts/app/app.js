(function($, Drupal) {

	'use strict';

	Drupal.behaviors.sidebarMenu = {
		attach: function (context, settings) {
      $('body', context).once('sidebar-menu').each(function () {
        // Open menu
        $('.menu-btn.menu-open').on('click', function() {
          $('body').addClass('sidebar-open');
        });
        // Close menu
        $('.close-menu').on('click', function() {
          $('body').removeClass('sidebar-open');
        });
      });
    }
	};

  // Read more in content field
  Drupal.behaviors.readMore = {
    attach: function (context, settings) {
      $('p.read-more').once('once').each(function () {

        // Get all content which should be under read more
        var html = '';
        $('p.read-more').nextAll().each(function () {
          html += $(this)[0]['outerHTML'];
          $(this).remove();
        });

        // Add wrapper around read more content
        var readMoreContent = '<div class="read-more-content">' + html + '</div>';
        $('p.read-more').after(readMoreContent);
      });

      // On click show rest of the content
      $('p.read-more').on('click', function () {
        $(this).next().slideDown();
        $(this).slideUp();
      });
    }
  };

  // Click on offer block on Angebot page
  Drupal.behaviors.offerBlockClick = {
    attach: function (context, settings) {
      $('.paragraph--type--offer-block-item', context).on('click', function () {
        if (!$(this).hasClass('active')) {
          $('.paragraph--type--offer-block-item.active').removeClass('active');
        }
        $(this).toggleClass('active');

      });
    }
  };

  // Validation for contact forms
  Drupal.behaviors.contactFormValidate = {
    attach: function (context, settings) {

      // Set left position of popup
      if ($('form.contact-form').length) {
        var formLeftPosition = $('form.contact-form').offset().left;
        var halfFormWidth = $('form.contact-form').width() / 2;
        var leftPopupPosition = formLeftPosition + halfFormWidth + "px";
        $('.form-error-popup').css("left", leftPopupPosition);

        $(window).on('resize', function () {
          var formLeftPosition = $('form.contact-form').offset().left;
          var halfFormWidth = $('form.contact-form').width() / 2;
          var leftPopupPosition = formLeftPosition + halfFormWidth + "px";
          $('.form-error-popup').css("left", leftPopupPosition);
        })
      }

      $('.form-error-popup .close-btn', context).on('click', function() {
        $('form.contact-form').removeClass('error-popup-active');
      });

    }
  };

  // Sticky menu
  Drupal.behaviors.stickyHeader = {
    attach: function(context, settings) {
      var lastPosition = 0;
      $(window).scroll(function() {
        if ($(document).scrollTop() > 55) {
          $('header.header').addClass('sticky');
        }
        else {
          $('header.header').removeClass('sticky');
        }

        if (lastPosition > $(document).scrollTop()) {
          $('header.header').addClass('sticky-open');
        }
        else {
          $('header.header').removeClass('sticky-open');
        }
        lastPosition = $(document).scrollTop();
      });
    }
  };

})(jQuery, Drupal);
