(function($) {

	'use strict';

	Drupal.behaviors.sidebarMenu = {
		attach: function (context, settings) {
      $('body', context).once('sidebar-menu').each(function () {
        // Open menu
        $('.menu-btn.menu-open').on('click', function() {
          $('body').addClass('sidebar-open');
        });
        // Close menu
        $('.close-menu').on('click', function() {
          $('body').removeClass('sidebar-open');
        });
      });
    }
	};
})(jQuery);

//# sourceMappingURL=../scripts/application.js.map
